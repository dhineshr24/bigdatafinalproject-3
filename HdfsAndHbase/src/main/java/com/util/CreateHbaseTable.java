package com.util;

import org.apache.hadoop.hbase.HColumnDescriptor;
import org.apache.hadoop.hbase.HTableDescriptor;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.Admin;
import org.apache.hadoop.hbase.client.Connection;

import java.io.IOException;
import java.util.ArrayList;

public class CreateHbaseTable {
    private Connection connection;
    private String hbaseTableNameToCreate;
    private ArrayList<String> columnFamilyList;

    public CreateHbaseTable(Connection connection, String hbaseTableNameToCreate, ArrayList<String> columnFamilyList) {
        this.connection = connection;
        this.hbaseTableNameToCreate = hbaseTableNameToCreate;
        this.columnFamilyList = columnFamilyList;
    }

    public void CreateHbaseTable() throws IOException {
        Admin admin = connection.getAdmin();
        HTableDescriptor tableName = new HTableDescriptor(TableName.valueOf(hbaseTableNameToCreate));
        for (String columnFam : columnFamilyList) {
            tableName.addFamily(new HColumnDescriptor(columnFam));
        }
        if (!admin.tableExists(tableName.getTableName())) {
            System.out.print("Creating table. ");
            admin.createTable(tableName);
            System.out.println(" Done.");
        }
    }

}
