package com.service.employeeleastattendance;

import com.util.protoobjects.EmployeeOuterClass;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.io.ImmutableBytesWritable;
import org.apache.hadoop.hbase.mapreduce.TableMapper;
import org.apache.hadoop.hbase.mapreduce.TableSplit;
import org.apache.hadoop.hbase.util.Bytes;
import org.apache.hadoop.io.IntWritable;

import java.util.Arrays;

public class AttendanceMapper extends TableMapper<IntWritable, ImmutableBytesWritable> {
    private static byte[] employeeTable = Bytes.toBytes("employee");
    public static final byte[] EmpCF = "employee_details".getBytes();
    public static final byte[] EmpATTR1 = "employee_qual".getBytes();

    @Override
    public void map(ImmutableBytesWritable rowKey, Result columns, Context context) {
        TableSplit currentSplit = (TableSplit) context.getInputSplit();
        byte[] tableName = currentSplit.getTableName();
        try {
            if (Arrays.equals(tableName, employeeTable)) {
                System.out.println("in if of employee");
                ImmutableBytesWritable value = new ImmutableBytesWritable();
                value.set(columns.getValue(EmpCF, EmpATTR1));
                EmployeeOuterClass.Employee.Builder employee =
                        EmployeeOuterClass.Employee.newBuilder().mergeFrom(value.get());

                int building_code = employee.getBuildingCode();
                context.write(new IntWritable(building_code),
                        new ImmutableBytesWritable(employee.build().toByteArray()));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
